# Retrofit 2
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# Retrofit 2 RxJava Adapter
-dontwarn retrofit2.adapter.rxjava.CompletableHelper$**

# OkHttp
-dontwarn okio.**

# SimpleXml
# Don't warn about stream.
-dontwarn org.simpleframework.xml.stream.**
# Keep all constructors inside SimpleXML, as it uses them via reflection.
-keepclassmembers class org.simpleframework.xml.** {
  public <init>(...);
}
# Do not delete annotated fields as per not throwing IllegalStateException (strict is true by default).
-keepclassmembers class * {
  @org.simpleframework.xml.* <fields>;
  @org.simpleframework.xml.* <init>(...);
}

# Models
-keepclassmembers class com.szagurskii.exchangerates.api.models.ExchangeRate {
  # Keep unused constructors as SimpleXML will use them.
  public <init>(...);
  # Keep all getter methods as SimpleXML will refer to them.
  *** get*(...);
}
-keepclassmembers class com.szagurskii.exchangerates.api.models.ExchangeRatesResponse {
  # Keep unused constructors as SimpleXML will use them.
  public <init>(...);
  # Keep all getter methods as SimpleXML will refer to them.
  *** get*(...);
}

# RxJava
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

# ViewPagerIndicator
-dontwarn com.viewpagerindicator.**

# EventBus
-keepattributes *Annotation*
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
