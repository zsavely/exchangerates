package com.szagurskii.exchangerates.network;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class OkHttpInterceptorsModule {
  @Provides @Singleton @NonNull
  public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
    HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return httpLoggingInterceptor;
  }

  @Provides @OkHttpInterceptors @Singleton @NonNull
  public List<Interceptor> provideOkHttpInterceptors(@NonNull HttpLoggingInterceptor httpLoggingInterceptor) {
    List<Interceptor> interceptors = new ArrayList<>();
    interceptors.add(httpLoggingInterceptor);
    return interceptors;
  }

  @Provides @OkHttpNetworkInterceptors @Singleton @NonNull
  public List<Interceptor> provideOkHttpNetworkInterceptors() {
    return Collections.emptyList();
  }
}
