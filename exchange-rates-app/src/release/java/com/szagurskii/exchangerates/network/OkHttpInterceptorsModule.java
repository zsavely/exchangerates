package com.szagurskii.exchangerates.network;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;

@Module
public class OkHttpInterceptorsModule {
  @Provides @OkHttpInterceptors @Singleton @NonNull
  public List<Interceptor> provideOkHttpInterceptors() {
    return Collections.emptyList();
  }

  @Provides @OkHttpNetworkInterceptors @Singleton @NonNull
  public List<Interceptor> provideOkHttpNetworkInterceptors() {
    return Collections.emptyList();
  }
}
