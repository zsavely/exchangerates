package com.szagurskii.exchangerates.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

import static com.szagurskii.exchangerates.api.models.ExchangeRatesResponse.PREFIX;
import static com.szagurskii.exchangerates.api.models.ExchangeRatesResponse.REFERENCE;

@Root(name = "Envelope", strict = false)
@Namespace(reference = REFERENCE, prefix = PREFIX)
public final class ExchangeRatesResponse {
  static final String REFERENCE = "http://www.gesmes.org/xml/2002-08-01";
  static final String PREFIX = "gesmes";

  private static final String SENDER_PATH = "Sender";
  private static final String CUBE_PATH = "Cube/Cube[1]";

  private static final String SUBJECT_ELEMENT_NAME = "subject";
  private static final String NAME_ELEMENT_NAME = "name";
  private static final String TIME_ELEMENT_NAME = "time";
  private static final String CUBE_ELEMENT_NAME = "Cube";

  private final String subject;
  private final String senderName;
  private final String cubeTime;
  private final List<ExchangeRate> cubes;

  public ExchangeRatesResponse(
      @Namespace(reference = REFERENCE, prefix = PREFIX)
      @Element(name = SUBJECT_ELEMENT_NAME)
          String subject,

      @Namespace(reference = REFERENCE, prefix = PREFIX)
      @Path(SENDER_PATH)
      @Element(name = NAME_ELEMENT_NAME)
          String senderName,

      @Path(CUBE_PATH)
      @Attribute(name = TIME_ELEMENT_NAME)
          String cubeTime,

      @Path(CUBE_PATH)
      @ElementList(entry = CUBE_ELEMENT_NAME, inline = true)
          List<ExchangeRate> cubes) {
    this.subject = subject;
    this.senderName = senderName;
    this.cubeTime = cubeTime;
    this.cubes = cubes;
  }

  @Namespace(reference = REFERENCE, prefix = PREFIX)
  @Element(name = SUBJECT_ELEMENT_NAME)
  public String getSubject() {
    return subject;
  }

  @Namespace(reference = REFERENCE, prefix = PREFIX)
  @Path(SENDER_PATH)
  @Element(name = NAME_ELEMENT_NAME)
  public String getSenderName() {
    return senderName;
  }

  @Path(CUBE_PATH)
  @Attribute(name = TIME_ELEMENT_NAME)
  public String getCubeTime() {
    return cubeTime;
  }

  @Path(CUBE_PATH)
  @ElementList(entry = CUBE_ELEMENT_NAME, inline = true)
  public List<ExchangeRate> getCubes() {
    return cubes;
  }

  @Override public String toString() {
    return "ExchangeRatesResponse{" +
        "subject='" + subject + '\'' +
        ", senderName='" + senderName + '\'' +
        ", cubeTime='" + cubeTime + '\'' +
        ", cubes=" + cubes +
        '}';
  }
}
