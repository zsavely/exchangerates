package com.szagurskii.exchangerates.api;

import com.szagurskii.exchangerates.BuildConfig;
import com.szagurskii.exchangerates.api.models.ExchangeRatesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

public interface ExchangeRatesApi {
  @GET(BuildConfig.API_URL) Call<ExchangeRatesResponse> rates();

  @GET(BuildConfig.API_URL) Observable<ExchangeRatesResponse> ratesRx();
}
