package com.szagurskii.exchangerates.api;

import android.support.annotation.NonNull;

import com.szagurskii.exchangerates.BuildConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class ApiModule {
  @Provides @NonNull @Singleton
  public ExchangeRatesApi provideClipdRestApi(@NonNull OkHttpClient okHttpClient) {
    return new Retrofit.Builder()
        .baseUrl("https://google.com") // Just a bump. One cannot set `null` baseUrl.
        .client(okHttpClient)
        .addConverterFactory(SimpleXmlConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .validateEagerly(BuildConfig.DEBUG)
        .build()
        .create(ExchangeRatesApi.class);
  }
}
