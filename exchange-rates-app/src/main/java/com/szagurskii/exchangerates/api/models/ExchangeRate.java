package com.szagurskii.exchangerates.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public final class ExchangeRate {
  private static final String CURRENCY = "currency";
  private static final String RATE = "rate";

  private final String currency;
  private final double rate;

  public ExchangeRate(@Attribute(name = CURRENCY) String currency,
      @Attribute(name = RATE) double rate) {
    this.currency = currency;
    this.rate = rate;
  }

  @Attribute(name = CURRENCY)
  public String getCurrency() {
    return currency;
  }

  @Attribute(name = RATE)
  public double getRate() {
    return rate;
  }

  @Override public String toString() {
    return "ExchangeRate{" +
        "currency='" + currency + '\'' +
        ", rate=" + rate +
        '}';
  }
}
