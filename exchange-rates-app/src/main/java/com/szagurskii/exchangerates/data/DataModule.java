package com.szagurskii.exchangerates.data;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import com.jakewharton.rx.transformer.ReplayingShare;
import com.szagurskii.exchangerates.api.ExchangeRatesApi;
import com.szagurskii.exchangerates.api.models.ExchangeRate;
import com.szagurskii.exchangerates.api.models.ExchangeRatesResponse;
import com.szagurskii.exchangerates.data.models.UserBalance;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

@Module
public class DataModule {
  private static final int ERROR_DELAY_IN_SECONDS = 3;
  private static final int SUCCESS_DELAY_IN_SECONDS = 30;

  public static final String EUR = "EUR";
  public static final String USD = "USD";
  public static final String GBP = "GBP";

  public static final String EXCHANGE_RATES_OBSERVABLE_NAMED = "exchange_rates_observable_named";
  public static final String EXCHANGE_RATES_NAMED = "exchange_rates_named";

  @Provides @NonNull @Singleton
  public UserBalance provideUserBalance() {
    Random random = new Random();
    UserBalance userBalance = new UserBalance();
    userBalance.userBalances().put(USD, UserBalance.Balance.create(USD, "$", random.nextInt(1000) * random.nextDouble()));
    userBalance.userBalances().put(EUR, UserBalance.Balance.create(EUR, "€", random.nextInt(1000) * random.nextDouble()));
    userBalance.userBalances().put(GBP, UserBalance.Balance.create(GBP, "£", random.nextInt(1000) * random.nextDouble()));
    return userBalance;
  }

  @Provides @NonNull @Singleton @Named(EXCHANGE_RATES_NAMED)
  public ArrayMap<String, ArrayMap<String, Double>> provideExchangeRates() {
    ArrayMap<String, ArrayMap<String, Double>> exchangeRates = new ArrayMap<>();

    exchangeRates.put(EUR, new ArrayMap<String, Double>());
    exchangeRates.put(USD, new ArrayMap<String, Double>());
    exchangeRates.put(GBP, new ArrayMap<String, Double>());

    exchangeRates.get(EUR).put(EUR, 1D);
    exchangeRates.get(USD).put(USD, 1D);
    exchangeRates.get(GBP).put(GBP, 1D);

    return exchangeRates;
  }

  @Provides @NonNull @Singleton @Named(EXCHANGE_RATES_OBSERVABLE_NAMED)
  public Observable<ArrayMap<String, ArrayMap<String, Double>>> provideExchangeRateObservable(@NonNull ExchangeRatesApi exchangeRatesApi,
      @Named(EXCHANGE_RATES_NAMED) final ArrayMap<String, ArrayMap<String, Double>> exchangeRates) {
    return exchangeRatesApi.ratesRx()
        .observeOn(Schedulers.computation())
        .subscribeOn(Schedulers.io())
        .map(new Func1<ExchangeRatesResponse, List<ExchangeRate>>() {
          @Override public List<ExchangeRate> call(ExchangeRatesResponse exchangeRatesResponse) {
            return exchangeRatesResponse.getCubes();
          }
        })
        .map(new Func1<List<ExchangeRate>, ArrayMap<String, ArrayMap<String, Double>>>() {
          @Override public ArrayMap<String, ArrayMap<String, Double>> call(List<ExchangeRate> exchangeRatesList) {

            double eurUsd = -1d;
            double eurGbp = -1d;

            for (ExchangeRate rate : exchangeRatesList) {
              if (USD.equals(rate.getCurrency())) {
                eurUsd = rate.getRate();
              }
              if (GBP.equals(rate.getCurrency())) {
                eurGbp = rate.getRate();
              }
            }

            double usdEur = 1d / eurUsd;
            double gbpEur = 1d / eurGbp;

            exchangeRates.get(USD).put(EUR, usdEur);
            exchangeRates.get(GBP).put(EUR, gbpEur);

            for (ExchangeRate rate : exchangeRatesList) {
              exchangeRates.get(EUR).put(rate.getCurrency(), rate.getRate());
              exchangeRates.get(USD).put(rate.getCurrency(), rate.getRate() * usdEur);
              exchangeRates.get(GBP).put(rate.getCurrency(), rate.getRate() * gbpEur);
            }

            return exchangeRates;
          }
        })
        .retryWhen(new Func1<Observable<? extends Throwable>, Observable<?>>() {
          @Override public Observable<?> call(Observable<? extends Throwable> observable) {
            return observable.flatMap(new Func1<Throwable, Observable<Long>>() {
              @Override public Observable<Long> call(Throwable throwable) {
                return Observable.timer(ERROR_DELAY_IN_SECONDS, TimeUnit.SECONDS);
              }
            });
          }
        })
        .repeatWhen(new Func1<Observable<? extends Void>, Observable<?>>() {
          @Override public Observable<?> call(Observable<? extends Void> observable) {
            return observable.delay(SUCCESS_DELAY_IN_SECONDS, TimeUnit.SECONDS);
          }
        })
        .compose(ReplayingShare.<ArrayMap<String, ArrayMap<String, Double>>>instance());
  }
}
