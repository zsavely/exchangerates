package com.szagurskii.exchangerates.data.events;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ValueChangedEvent {
  @NonNull public abstract String id();

  @NonNull public abstract String currency();

  @NonNull public abstract String oppositeCurrency();

  @Nullable public abstract String value();

  public static ValueChangedEvent create(String id, String currency, String oppositeCurrency, String value) {
    return new AutoValue_ValueChangedEvent(id, currency, oppositeCurrency, value);
  }
}
