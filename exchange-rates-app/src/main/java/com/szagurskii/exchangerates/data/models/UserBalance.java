package com.szagurskii.exchangerates.data.models;

import android.support.v4.util.ArrayMap;

import com.google.auto.value.AutoValue;

public final class UserBalance {
  private final ArrayMap<String, Balance> userBalances = new ArrayMap<>();

  public ArrayMap<String, Balance> userBalances() {
    return userBalances;
  }

  @AutoValue public abstract static class Balance {
    public abstract String currencyName();

    public abstract String currencySymbol();

    public abstract Double balance();

    public static Balance create(String currencyName, String currencySymbol, Double balance) {
      return new AutoValue_UserBalance_Balance(currencyName, currencySymbol, balance);
    }
  }
}
