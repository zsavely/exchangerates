package com.szagurskii.exchangerates.ui.exchangerates;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.szagurskii.exchangerates.ExchangeRatesApp;
import com.szagurskii.exchangerates.R;
import com.szagurskii.exchangerates.data.events.ValueChangedEvent;
import com.szagurskii.exchangerates.databinding.FragmentExchangeBinding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.internal.Preconditions;

public class ExchangeFragment extends Fragment implements ExchangeView {
  private static final String CURRENCY_NAME_KEY = "currency_name_key";
  private static final String ACTIVE_KEY = "active_key";

  @BindView(R.id.et_exchange_value) EditText exchangeValueView;
  @Inject ExchangePresenter exchangePresenter;
  private Unbinder unbinder;

  private final ExchangeBinding exchangeBinding = new ExchangeBinding();

  static ExchangeFragment exchangeFragment(@NonNull String currencyName, boolean isActive) {
    ExchangeFragment exchangeFragment = new ExchangeFragment();
    Bundle bundle = new Bundle();
    bundle.putString(CURRENCY_NAME_KEY, currencyName);
    bundle.putBoolean(ACTIVE_KEY, isActive);
    exchangeFragment.setArguments(bundle);
    return exchangeFragment;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    ExchangeRatesApp.get(getContext()).applicationComponent().inject(this);
    Preconditions.checkNotNull(exchangePresenter, "No injection has been made!");

    boolean isActive = getArguments().getBoolean(ACTIVE_KEY, false);
    String currency = getArguments().getString(CURRENCY_NAME_KEY, "UNKNOWN");
    exchangePresenter.setActive(isActive);
    exchangePresenter.setCurrency(currency);
    exchangePresenter.setOppositeCurrency(currency);
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    FragmentExchangeBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exchange, container, false);
    View view = binding.getRoot();
    unbinder = ButterKnife.bind(this, view);
    binding.setExchange(exchangeBinding);
    return view;
  }

  @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    exchangePresenter.bindView(this);
  }

  @Override public void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Override public void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Override public void onDestroyView() {
    exchangePresenter.unbindView(this);
    super.onDestroyView();
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override public void setCurrentCurrencySymbol(@NonNull String currencySymbol) {
    exchangeBinding.currentCurrencySymbol.set(currencySymbol);
  }

  @Override public void setCurrentCurrencyName(@NonNull String currencyName) {
    exchangeBinding.currentCurrencyName.set(currencyName);
  }

  @Override public void setCurrentBalance(@NonNull String currentBalance) {
    exchangeBinding.currentFormattedBalance.set(currentBalance);
  }

  @Override public void setComparedCurrencySymbol(@NonNull String currencySymbol) {
    exchangeBinding.comparedCurrencySymbol.set(currencySymbol);
  }

  @Override public void setComparedCurrencyName(@NonNull String currencyName) {
    exchangeBinding.comparedCurrencyName.set(currencyName);
  }

  @Override public void setComparedCurrencyValue(@Nullable String comparedCurrencyValue) {
    exchangeBinding.comparedCurrencyValue.set(comparedCurrencyValue);
  }

  @Override public void setCurrencyResultConversion(@Nullable String value) {
    exchangeValueView.setText(value);
  }

  @Override public boolean isEditTextFocused() {
    return exchangeValueView.isFocused();
  }

  @NonNull @Override public EditText exchangeView() {
    return exchangeValueView;
  }

  @NonNull @Override public String value() {
    return exchangeValueView.getText().toString();
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onValueChangedEvent(ValueChangedEvent event) {
    if (presenter().isActive() &&
        event.currency().equals(presenter().oppositeCurrency()) &&
        event.oppositeCurrency().equals(presenter().currency()) &&
        !event.id().equals(presenter().id())) {
      setCurrencyResultConversion(event.value());
    }
  }

  public ExchangePresenter presenter() {
    return exchangePresenter;
  }
}
