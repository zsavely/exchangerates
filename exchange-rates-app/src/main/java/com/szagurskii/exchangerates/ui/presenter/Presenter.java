package com.szagurskii.exchangerates.ui.presenter;

import android.support.annotation.NonNull;

public interface Presenter<V> {
  void bindView(@NonNull V view);

  void unbindView(@NonNull V view);
}
