package com.szagurskii.exchangerates.ui.exchangerates;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.EditText;

public interface ExchangeView {
  //@formatter:off
  void setCurrentCurrencySymbol(@NonNull String currencySymbol);
  void setCurrentCurrencyName(@NonNull String currencyName);
  void setCurrentBalance(@NonNull String currentBalance);

  void setComparedCurrencySymbol(@NonNull String currencySymbol);
  void setComparedCurrencyName(@NonNull String currencyName);
  void setComparedCurrencyValue(@Nullable String comparedCurrencyValue);
  void setCurrencyResultConversion(@NonNull String value);

  boolean isEditTextFocused();
  @NonNull EditText exchangeView();
  @NonNull String value();
  //@formatter:on
}
