package com.szagurskii.exchangerates.ui.exchangerates;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.szagurskii.exchangerates.R;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.szagurskii.exchangerates.data.DataModule.EUR;
import static com.szagurskii.exchangerates.data.DataModule.GBP;
import static com.szagurskii.exchangerates.data.DataModule.USD;

public class ExchangeActivity extends AppCompatActivity {

  @BindView(R.id.vp_top) ViewPager topViewPager;
  @BindView(R.id.vp_bottom) ViewPager bottomViewPager;

  @BindView(R.id.cpi_top) CirclePageIndicator topIndicatorView;
  @BindView(R.id.cpi_bottom) CirclePageIndicator bottomIndicatorView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    final ExchangeFragment[] topFragments = new ExchangeFragment[]{
        ExchangeFragment.exchangeFragment(USD, true),
        ExchangeFragment.exchangeFragment(EUR, false),
        ExchangeFragment.exchangeFragment(GBP, false)
    };
    ExchangePagerAdapter topAdapter = new ExchangePagerAdapter(
        getSupportFragmentManager(),
        topFragments
    );

    final ExchangeFragment[] bottomFragments = new ExchangeFragment[]{
        ExchangeFragment.exchangeFragment(USD, true),
        ExchangeFragment.exchangeFragment(EUR, false),
        ExchangeFragment.exchangeFragment(GBP, false)
    };
    ExchangePagerAdapter bottomAdapter = new ExchangePagerAdapter(
        getSupportFragmentManager(),
        bottomFragments
    );

    topViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      @Override public void onPageSelected(int position) {
        for (int i = 0; i < topFragments.length; i++) {
          topFragments[i].presenter().onActiveChanged(topViewPager.getCurrentItem() == i);
        }

        topFragments[position].presenter().onPageChanged(
            bottomFragments[bottomViewPager.getCurrentItem()].presenter().currency(),
            bottomFragments[bottomViewPager.getCurrentItem()].presenter().currencySymbol()
        );

        bottomFragments[bottomViewPager.getCurrentItem()].presenter().onOppositePageChanged(
            topFragments[position].presenter().currency(),
            topFragments[position].presenter().currencySymbol()
        );
      }
    });

    bottomViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
      @Override public void onPageSelected(int position) {
        for (int i = 0; i < bottomFragments.length; i++) {
          bottomFragments[i].presenter().onActiveChanged(bottomViewPager.getCurrentItem() == i);
        }

        bottomFragments[position].presenter().onPageChanged(
            topFragments[topViewPager.getCurrentItem()].presenter().currency(),
            topFragments[topViewPager.getCurrentItem()].presenter().currencySymbol()
        );

        topFragments[topViewPager.getCurrentItem()].presenter().onOppositePageChanged(
            bottomFragments[position].presenter().currency(),
            bottomFragments[position].presenter().currencySymbol()
        );
      }
    });

    topViewPager.setOffscreenPageLimit(2);
    bottomViewPager.setOffscreenPageLimit(2);

    topViewPager.setAdapter(topAdapter);
    bottomViewPager.setAdapter(bottomAdapter);

    topIndicatorView.setViewPager(topViewPager);
    bottomIndicatorView.setViewPager(bottomViewPager);
  }
}
