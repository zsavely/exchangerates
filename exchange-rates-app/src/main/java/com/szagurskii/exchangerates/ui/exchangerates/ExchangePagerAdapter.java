package com.szagurskii.exchangerates.ui.exchangerates;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExchangePagerAdapter extends FragmentStatePagerAdapter {
  private final List<Fragment> fragmentsList = new ArrayList<>(3);

  public ExchangePagerAdapter(FragmentManager fm, Fragment... fragments) {
    super(fm);
    Collections.addAll(fragmentsList, fragments);
  }

  @Override public Fragment getItem(int position) {
    return fragmentsList.get(position);
  }

  @Override public int getCount() {
    return fragmentsList.size();
  }
}
