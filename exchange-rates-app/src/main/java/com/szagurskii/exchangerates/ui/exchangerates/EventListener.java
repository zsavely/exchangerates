package com.szagurskii.exchangerates.ui.exchangerates;

import android.support.annotation.NonNull;

public interface EventListener {
  /** Indicate that current fragment is now active. */
  void onActiveChanged(boolean isActive);

  /**
   * This event happens when current fragment has been just switched to in the ViewPager.
   *
   * @param oppositeCurrencyName   the opposite currency that is now shown in the opposite fragment.
   * @param oppositeCurrencySymbol the opposite currency that is now shown in the opposite fragment.
   */
  void onPageChanged(@NonNull String oppositeCurrencyName, @NonNull String oppositeCurrencySymbol);

  /**
   * This event happens when the opposite fragment is changed.
   *
   * @param newCurrencyName   the new currency of the opposite fragment.
   * @param newCurrencySymbol the new currency of the opposite fragment.
   */
  void onOppositePageChanged(@NonNull String newCurrencyName, @NonNull String newCurrencySymbol);

  /** Current Fragment's currency. */
  @NonNull String currency();

  /** Current Fragment's currency symbol. */
  @NonNull String currencySymbol();
}
