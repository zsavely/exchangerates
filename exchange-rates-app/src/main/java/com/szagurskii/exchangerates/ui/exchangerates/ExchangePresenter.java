package com.szagurskii.exchangerates.ui.exchangerates;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;
import com.szagurskii.exchangerates.data.events.ValueChangedEvent;
import com.szagurskii.exchangerates.data.models.UserBalance;
import com.szagurskii.exchangerates.ui.presenter.PresenterImpl;

import org.apache.commons.lang3.math.NumberUtils;
import org.greenrobot.eventbus.EventBus;

import java.text.NumberFormat;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static com.szagurskii.exchangerates.data.DataModule.EXCHANGE_RATES_NAMED;
import static com.szagurskii.exchangerates.data.DataModule.EXCHANGE_RATES_OBSERVABLE_NAMED;

public class ExchangePresenter extends PresenterImpl<ExchangeView> implements Observer<ArrayMap<String, ArrayMap<String, Double>>>, EventListener {

  private final Observable<ArrayMap<String, ArrayMap<String, Double>>> exchangeRatesObservable;
  private final UserBalance userBalance;
  final ArrayMap<String, ArrayMap<String, Double>> exchangeRates;

  @NonNull final String id;
  volatile boolean isActive;
  volatile String currency;
  volatile String oppositeCurrency;

  final NumberFormat numberFormatter = NumberFormat.getNumberInstance();

  @Inject public ExchangePresenter(
      @Named(EXCHANGE_RATES_OBSERVABLE_NAMED) Observable<ArrayMap<String, ArrayMap<String, Double>>> exchangeRatesObservable,
      @Named(EXCHANGE_RATES_NAMED) ArrayMap<String, ArrayMap<String, Double>> exchangeRates,
      UserBalance userBalance) {
    this.exchangeRatesObservable = exchangeRatesObservable;
    this.exchangeRates = exchangeRates;
    this.userBalance = userBalance;
    this.id = UUID.randomUUID().toString();

    numberFormatter.setGroupingUsed(false);
    numberFormatter.setMaximumFractionDigits(2);
  }

  @Override public void bindView(@NonNull final ExchangeView view) {
    super.bindView(view);

    view.setCurrentCurrencySymbol(userBalance.userBalances().get(currency).currencySymbol());
    view.setCurrentCurrencyName(currency);
    view.setCurrentBalance(numberFormatter.format(userBalance.userBalances().get(currency).balance()));

    view.setComparedCurrencySymbol(userBalance.userBalances().get(currency).currencySymbol());
    view.setComparedCurrencyName(currency);
    view.setComparedCurrencyValue(format(currency));

    unsubscribeOnUnbindView(
        exchangeRatesObservable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this)
    );

    unsubscribeOnUnbindView(
        subscription(
            RxTextView.afterTextChangeEvents(view.exchangeView())
                .subscribeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<TextViewAfterTextChangeEvent, Boolean>() {
                  @Override public Boolean call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                    return isActive;
                  }
                })
                .filter(new Func1<TextViewAfterTextChangeEvent, Boolean>() {
                  @Override public Boolean call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                    ExchangeView exchangeView = view();
                    return exchangeView != null && exchangeView.isEditTextFocused();
                  }
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.computation())
                .map(new Func1<TextViewAfterTextChangeEvent, String>() {
                  @Override public String call(TextViewAfterTextChangeEvent textViewAfterTextChangeEvent) {
                    return textViewAfterTextChangeEvent.editable().toString();
                  }
                })
        )
    );
  }

  @Override public void unbindView(@NonNull ExchangeView view) {
    super.unbindView(view);
  }

  @Override public void onCompleted() {
    // Should never be called.
    throw new AssertionError("This observable should never stop.");
  }

  @Override public void onError(Throwable e) {
    // Should continue despite errors.
    Timber.d(e, "Error on observable.");
  }

  @Override public void onNext(ArrayMap<String, ArrayMap<String, Double>> exchangeRates) {
    Timber.d("Received exchange rates.");
  }

  @Override public void onActiveChanged(boolean isActive) {
    this.isActive = isActive;
  }

  @Override public void onPageChanged(@NonNull String oppositeCurrencyName, @NonNull String oppositeCurrencySymbol) {
    setOppositeCurrency(oppositeCurrencyName);
    updateValues(oppositeCurrencyName, oppositeCurrencySymbol);
  }

  @Override public void onOppositePageChanged(@NonNull String newCurrencyName, @NonNull String newCurrencySymbol) {
    setOppositeCurrency(newCurrencyName);
    updateValues(newCurrencyName, newCurrencySymbol);

    ExchangeView view = view();
    if (view != null && isActive()) {
      unsubscribeOnUnbindView(
          subscription(
              Observable.just(view.value())
                  .observeOn(Schedulers.computation())
                  .subscribeOn(Schedulers.computation())
          )
      );
    }
  }

  private void updateValues(@NonNull String currencyName, @NonNull String currencySymbol) {
    ExchangeView view = view();
    if (view != null) {
      view.setComparedCurrencyName(currencyName);
      view.setComparedCurrencySymbol(currencySymbol);
      view.setComparedCurrencyValue(format(currencyName));
    }
  }

  @Nullable private String format(@NonNull String currencyName) {
    Double value = exchangeRates.get(currency).get(currencyName);
    if (value == null) {
      return null;
    }
    return numberFormatter.format(value);
  }

  @NonNull public String id() {
    return id;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  @NonNull public String oppositeCurrency() {
    return oppositeCurrency;
  }

  public void setOppositeCurrency(@NonNull String oppositeCurrency) {
    this.oppositeCurrency = oppositeCurrency;
  }

  @NonNull @Override public String currency() {
    return currency;
  }

  public void setCurrency(@NonNull String currency) {
    this.currency = currency;
  }

  @NonNull @Override public String currencySymbol() {
    return userBalance.userBalances().get(currency).currencySymbol();
  }

  @NonNull private Subscription subscription(Observable<String> observable) {
    return observable
        .map(new Func1<String, Double>() {
          @Override public Double call(String value) {
            if (value.length() != 0 && NumberUtils.isCreatable(value)) {
              Double enteredValue = Double.valueOf(value);
              Double conversionRate = exchangeRates.get(currency).get(oppositeCurrency);
              if (enteredValue != null && conversionRate != null)
                return enteredValue * conversionRate;
            }
            return null;
          }
        })
        .map(new Func1<Double, String>() {
          @Override public String call(Double aDouble) {
            if (aDouble == null) return null;
            return numberFormatter.format(aDouble);
          }
        })
        .retryWhen(new Func1<Observable<? extends Throwable>, Observable<?>>() {
          @Override public Observable<?> call(Observable<? extends Throwable> observable) {
            return observable.switchMap(new Func1<Throwable, Observable<Long>>() {
              @Override public Observable<Long> call(Throwable throwable) {
                Timber.d(throwable);
                return Observable.timer(3, TimeUnit.SECONDS);
              }
            });
          }
        })
        .subscribe(new Observer<String>() {
          @Override public void onCompleted() {
          }

          @Override public void onError(Throwable e) {
            Timber.d(e, "Observing text changed events failed.");
          }

          @Override public void onNext(String value) {
            EventBus.getDefault().post(ValueChangedEvent.create(id, currency, oppositeCurrency, value));
          }
        });
  }
}
