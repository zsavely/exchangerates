package com.szagurskii.exchangerates.ui.exchangerates;

import android.databinding.ObservableField;

public final class ExchangeBinding {
  public final ObservableField<String> currentCurrencySymbol = new ObservableField<>();
  public final ObservableField<String> currentCurrencyName = new ObservableField<>();
  public final ObservableField<String> currentFormattedBalance = new ObservableField<>();

  public final ObservableField<String> comparedCurrencySymbol = new ObservableField<>();
  public final ObservableField<String> comparedCurrencyName = new ObservableField<>();
  public final ObservableField<String> comparedCurrencyValue = new ObservableField<>();
}
