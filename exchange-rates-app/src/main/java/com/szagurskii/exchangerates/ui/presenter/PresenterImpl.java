package com.szagurskii.exchangerates.ui.presenter;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public abstract class PresenterImpl<V> implements Presenter<V> {
  @NonNull private final CompositeSubscription unsubscribeList = new CompositeSubscription();
  @Nullable private volatile V view;

  @Override @CallSuper public void bindView(@NonNull V view) {
    final V previousView = this.view;

    if (previousView != null) {
      Timber.w(new IllegalStateException("Previous view is not unbounded! previousView = " + previousView), "bindView");
    }

    this.view = view;
  }

  @Override @CallSuper public void unbindView(@NonNull V view) {
    final V previousView = this.view;

    this.view = null;

    if (previousView != view) {
      Timber.w(new IllegalStateException("Unexpected view! previousView = " + previousView + ", view to unbind = " + view), "unbindView");
    }

    // Unsubscribe all subscriptions that need to be unsubscribed in this lifecycle state.
    unsubscribeList.clear();
  }

  @Nullable protected V view() {
    return view;
  }

  protected final void unsubscribeOnUnbindView(@NonNull Subscription subscription, @NonNull Subscription... subscriptions) {
    unsubscribeList.add(subscription);

    for (Subscription s : subscriptions) {
      unsubscribeList.add(s);
    }
  }
}
