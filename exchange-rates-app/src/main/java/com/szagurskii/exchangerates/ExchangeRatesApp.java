package com.szagurskii.exchangerates;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import timber.log.Timber;

public class ExchangeRatesApp extends Application {
  private ApplicationComponent applicationComponent;

  @Override public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
  }

  @NonNull public ApplicationComponent applicationComponent() {
    if (applicationComponent == null) {
      applicationComponent = prepareAppComponent().build();
    }
    return applicationComponent;
  }

  protected DaggerApplicationComponent.Builder prepareAppComponent() {
    return DaggerApplicationComponent.builder();
  }

  @NonNull public static ExchangeRatesApp get(@NonNull Context context) {
    return (ExchangeRatesApp) context.getApplicationContext();
  }
}
