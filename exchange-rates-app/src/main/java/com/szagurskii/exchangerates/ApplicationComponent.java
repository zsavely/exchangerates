package com.szagurskii.exchangerates;

import com.szagurskii.exchangerates.api.ApiModule;
import com.szagurskii.exchangerates.data.DataModule;
import com.szagurskii.exchangerates.network.NetworkModule;
import com.szagurskii.exchangerates.network.OkHttpInterceptorsModule;
import com.szagurskii.exchangerates.ui.exchangerates.ExchangeActivity;
import com.szagurskii.exchangerates.ui.exchangerates.ExchangeFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {
    ApplicationModule.class,
    ApiModule.class,
    NetworkModule.class,
    OkHttpInterceptorsModule.class,
    DataModule.class
})
@Singleton
public interface ApplicationComponent {
  void inject(ExchangeActivity exchangeActivity);

  void inject(ExchangeFragment exchangeFragment);
}
