package com.szagurskii.exchangerates;

import android.support.annotation.Nullable;

import com.szagurskii.exchangerates.api.models.ExchangeRate;
import com.szagurskii.exchangerates.api.models.ExchangeRatesResponse;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class SampleXmlParsingTest {
  private static final String ASSET_FILENAME = "eurofxref-daily.xml";

  @Test public void shouldLoadSampleXml() throws Exception {
    Assert.assertNotNull("Read XML is null!", sampleXml());
  }

  @Test public void shouldParseSuccessfully() throws Exception {
    Serializer serializer = new Persister();
    ExchangeRatesResponse exchangeRatesResponse = serializer.read(ExchangeRatesResponse.class, xmlInputStream());

    Assert.assertNotNull("exchangeRatesResponse is null!", exchangeRatesResponse);

    Assert.assertNotNull("exchangeRatesResponse.getCubeTime() is null!", exchangeRatesResponse.getCubeTime());
    Assert.assertEquals("2016-11-11", exchangeRatesResponse.getCubeTime());

    Assert.assertNotNull("exchangeRatesResponse.getSenderName() is null!", exchangeRatesResponse.getSenderName());
    Assert.assertEquals("European Central Bank", exchangeRatesResponse.getSenderName());

    Assert.assertNotNull("exchangeRatesResponse.getSubject() is null!", exchangeRatesResponse.getSubject());
    Assert.assertEquals("Reference rates", exchangeRatesResponse.getSubject());

    Assert.assertNotNull("exchangeRatesResponse.getCubes() is null!", exchangeRatesResponse.getCubes());

    Iterator<ExchangeRate> iterator = exchangeRatesResponse.getCubes().iterator();
    ExchangeRate exchangeRateFirst = iterator.next();
    Assert.assertEquals("USD", exchangeRateFirst.getCurrency());
    Assert.assertEquals(1.0904, exchangeRateFirst.getRate(), 0d);

    while (iterator.hasNext()) {
      ExchangeRate exchangeRate = iterator.next();
      Assert.assertNotNull("exchangeRate.getCurrency() is null!", exchangeRate.getCurrency());
      Assert.assertNotNull("exchangeRate.getRate() is null!", exchangeRate.getRate());
    }
  }

  @Nullable private String sampleXml() throws IOException {
    InputStream inputStream = xmlInputStream();
    String xml = IOUtils.toString(inputStream, "UTF-8");
    IOUtils.closeQuietly(inputStream);
    return xml;
  }

  @Nullable private InputStream xmlInputStream() throws IOException {
    return getClass().getClassLoader().getResourceAsStream(ASSET_FILENAME);
  }
}
